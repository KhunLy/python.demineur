from ctypes import *
from os import system, name
import msvcrt

 
STD_OUTPUT_HANDLE = -11
 
class COORD(Structure):
    pass
 
COORD._fields_ = [("X", c_short), ("Y", c_short)]
 
def print_at(x, y, s):
    h = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
    windll.kernel32.SetConsoleCursorPosition(h, COORD(x, y))
    print(str(s))

def set_cursor_at(x, y):
    h = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
    windll.kernel32.SetConsoleCursorPosition(h, COORD(x, y))

def clear():
    if name == "nt":
        system("cls")
    else:
        system("clear")

def getch():
    return msvcrt.getch()

def str_red(value):
    return '\033[91m'+str(value)+'\033[0m'

def str_green(value):
    return '\033[92m'+str(value)+'\033[0m'

def str_blue(value):
    return '\033[94m'+str(value)+'\033[0m'

def str_yellow(value):
    return '\033[93m'+str(value)+'\033[0m'

def str_purple(value):
    return '\033[95m'+str(value)+'\033[0m'

def str_cyan(value):
    return '\033[96m'+str(value)+'\033[0m'

def str_gray(value):
    return '\033[97m'+str(value)+'\033[0m' 

def str_black(value):
    return '\033[98m'+str(value)+'\033[0m' 

def str_underline(value):
    return '\033[4m'+str(value)+'\033[0m'

def str_bold(value):
    return '\033[1m'+str(value)+'\033[0m'
