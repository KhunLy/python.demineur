# coding: utf-8

from console import *
import random

class Tile:
    """
    state
    0: none
    1: visited
    2: marked as ?
    3: marked as Bomb
    """ 
    def __init__(self):
        self.value = 0
        self.state = 0

    def display(self):
        return self.value if self.state else "▓"

class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

nb_cols = int(input("Entrez un nombre de colonnes\n"))
nb_lines = int(input("Entrez un nombre de lignes\n"))
nb_bombs = int(input("Entrez un nombre de bombes\n"))
total_visited = 0
result = "Gagné"

mines_field = [[Tile() for i in range(nb_lines)] for j in range(nb_cols)]

def color(value):
    if value == 0:
        value = " "
    elif value == 1:
        value = str_blue(value)
    elif value == 2:
        value = str_green(value)
    elif value == 3:
        value = str_red(value)
    elif value == 4:
        value = str_purple(value)
    elif value == 5:
        value = str_yellow(value)
    elif value == 6:
        value = str_cyan(value)
    elif value == 7 or value == 8:
        value = str_black(value)
    return str(value)

def display_field():
    for x, col in enumerate(mines_field):
        for y, tile in enumerate(col):
            print_at(x * 2, y, color(tile.display()) + str_bold("|"))

def check(x,y):
    tile = mines_field[x][y]
    if tile.state != 0:
        return
    global total_visited
    total_visited += 1
    tile.state = 1
    print_at(x * 2, y, color(tile.display()))
    if tile.value == 0:
        for i in range(9):
            if (y - 1 + i//3 >= 0) and (y - 1 + i//3 < nb_lines) and (x - 1 + i%3 >= 0) and (x - 1 + i%3 < nb_cols) and (mines_field[x - 1 + i%3][y - 1 + i//3].value != '*'):
                check(x - 1 + i%3, y - 1 + i//3)
    return tile.value != "*"

added_bombs = 0

while added_bombs < nb_bombs:
    x = int(random.random() * nb_cols)
    y = int(random.random() * nb_lines)
    while mines_field[x][y].value == '*':
        x = int(random.random() * nb_cols)
        y = int(random.random() * nb_lines)
    mines_field[x][y].value = '*'
    for i in range(9):
        if (y - 1 + i//3 >= 0) and (y - 1 + i//3 < nb_lines) and (x - 1 + i%3 >= 0) and (x - 1 + i%3 < nb_cols) and (mines_field[x - 1 + i%3][y - 1 + i//3].value != '*'):
            mines_field[x - 1 + i%3][y - 1 + i//3].value += 1
    added_bombs += 1

clear()
display_field()

cursor_pos = Position(0,0)

while total_visited + nb_bombs < nb_cols * nb_lines:
    set_cursor_at(cursor_pos.x * 2, cursor_pos.y)
    dir = getch()
    if dir == b'\xe0':
        continue
    if dir == b'M':
        cursor_pos.x = (cursor_pos.x + 1) % nb_cols
    elif dir == b'K':
        cursor_pos.x = (cursor_pos.x - 1) % nb_cols
    elif dir == b'H':
        cursor_pos.y = (cursor_pos.y - 1) % nb_lines
    elif dir == b'P':
        cursor_pos.y = (cursor_pos.y + 1) % nb_lines
    elif dir == b' ':
        if not check(cursor_pos.x,cursor_pos.y):
            result = "GAME OVER"
            break
        set_cursor_at(cursor_pos.x * 2, cursor_pos.y)
print_at(0, nb_lines + 2, result + "\n")
        

            